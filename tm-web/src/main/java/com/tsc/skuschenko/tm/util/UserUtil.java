package com.tsc.skuschenko.tm.util;

import com.tsc.skuschenko.tm.exception.entity.user.AccessDeniedException;
import com.tsc.skuschenko.tm.model.CustomerUser;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public class UserUtil {

    public static String getUserId() {
        @NotNull final Authentication auth = SecurityContextHolder
                .getContext()
                .getAuthentication();
        @Nullable Object principal = auth.getPrincipal();
        Optional.ofNullable(principal).orElseThrow(AccessDeniedException::new);
        if (!(principal instanceof CustomerUser)) {
            throw new AccessDeniedException();
        }
        @NotNull CustomerUser customerUser = (CustomerUser) principal;
        return customerUser.getUserId();
    }

}

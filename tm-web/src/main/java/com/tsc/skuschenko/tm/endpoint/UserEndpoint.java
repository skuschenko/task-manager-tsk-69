package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.endpoint.IUserRestEndpoint;
import com.tsc.skuschenko.tm.api.service.IUserService;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/users")
@WebService(
        endpointInterface = "com.tsc.skuschenko.tm.api.endpoint." +
                "IUserRestEndpoint"
)
public class UserEndpoint implements IUserRestEndpoint {

    @NotNull
    static final String CREATE_METHOD = "/create";

    @NotNull
    static final String DELETE_BY_ID_METHOD = "/deleteById/{id}";

    @NotNull
    static final String FIND_ALL_METHOD = "/findAll";

    @NotNull
    static final String FIND_BY_ID_METHOD = "/findById/{id}";

    @NotNull
    static final String SAVE_METHOD = "/save";

    @Autowired
    @NotNull
    private IUserService userService;

    @Override
    @WebMethod
    @PostMapping(CREATE_METHOD)
    public void create(
            @WebParam(name = "user")
            @RequestBody @NotNull final User user
    ) {
        userService.save(user);
    }

    @Override
    @WebMethod
    @DeleteMapping(DELETE_BY_ID_METHOD)
    public void deleteByLogin(
            @WebParam(name = "login")
            @PathVariable("login") @NotNull final String login
    ) {
        userService.removeByLogin(login);
    }

    @Override
    @WebMethod
    @GetMapping(FIND_BY_ID_METHOD)
    public User find(
            @WebParam(name = "id")
            @PathVariable("id") @NotNull final String id
    ) {
        return userService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping(FIND_ALL_METHOD)
    public Collection<User> findAll() {
        return userService.findAll();
    }

    @Override
    @WebMethod
    @PutMapping(SAVE_METHOD)
    public void save(
            @WebParam(name = "user")
            @RequestBody @NotNull final User user
    ) {
        userService.save(user);
    }

}

package com.tsc.skuschenko.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface IPropertyService {

    @Nullable String getFactoryClass();

    @Nullable String getJdbcDialect();

    @Nullable
    String getJdbcDriver();

    @Nullable String getJdbcHbm2ddl();

    @Nullable
    String getJdbcPassword();

    @Nullable String getJdbcShowSql();

    @Nullable
    String getJdbcUrl();

    @Nullable
    String getJdbcUserName();

    @Nullable String getLiteMember();

    @Nullable String getMinimalPuts();

    String getNeedSecondLevel();

    @Nullable String getProviderConfiguration();

    @Nullable String getQueryCache();

    @Nullable String getRegionPrefix();

    @Nullable String getSecondLevel();

}

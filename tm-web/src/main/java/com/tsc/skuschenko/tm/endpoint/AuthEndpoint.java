package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.endpoint.IAuthRestEndpoint;
import com.tsc.skuschenko.tm.api.service.IUserService;
import com.tsc.skuschenko.tm.exception.entity.user.UserNotFoundException;
import com.tsc.skuschenko.tm.model.Result;
import com.tsc.skuschenko.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import javax.jws.WebParam;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/auth")
public class AuthEndpoint implements IAuthRestEndpoint {

    @NotNull
    static final String LOGIN = "/login";

    @NotNull
    static final String LOGOUT = "/logout";

    @NotNull
    static final String PROFILE = "/profile";

    @NotNull
    static final String SESSION = "/session";

    @Resource
    @NotNull
    private AuthenticationManager authenticationManager;

    @Autowired
    @NotNull
    private IUserService userService;

    @Override
    @GetMapping(LOGIN)
    public Result login(
            @WebParam(name = "username")
            @RequestParam("username") @NotNull final String username,
            @WebParam(name = "password")
            @RequestParam("password") @NotNull final String password) {
        try {
            @Nullable final User user = userService.findByLogin(username);
            Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
            List<SimpleGrantedAuthority> authorities = new ArrayList<>();
            user.getRoles().forEach(item -> authorities.add(
                    new SimpleGrantedAuthority(
                            "ROLE_" + item.getRoleType().name()))
            );
            @NotNull final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(
                            username,
                            password,
                            authorities
                    );
            @NotNull final Authentication authentication =
                    authenticationManager.authenticate(token);
            SecurityContextHolder
                    .getContext()
                    .setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (@NotNull final Exception e) {
            return new Result(e);
        }
    }

    @Override
    @GetMapping(value = LOGOUT, produces = "application/json")
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

    @Override
    @GetMapping(value = PROFILE, produces = "application/json")
    public User profile() {
        @NotNull final SecurityContext securityContext =
                SecurityContextHolder.getContext();
        @NotNull final Authentication authentication =
                securityContext.getAuthentication();
        return userService.findByLogin(authentication.getName());
    }

    @Override
    @GetMapping(value = SESSION, produces = "application/json")
    public Authentication session() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

}

package com.tsc.skuschenko.tm.configuration;

import com.tsc.skuschenko.tm.endpoint.AuthEndpointSOAP;
import com.tsc.skuschenko.tm.endpoint.ProjectEndpoint;
import com.tsc.skuschenko.tm.endpoint.TaskEndpoint;
import com.tsc.skuschenko.tm.endpoint.UserEndpoint;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.xml.ws.Endpoint;

@EnableWebMvc
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        prePostEnabled = true, jsr250Enabled = true, securedEnabled = true
)
@Configuration
@ComponentScan("com.tsc.skuschenko.tm")
@EnableGlobalAuthentication
public class WebApplicationConfiguration extends WebSecurityConfigurerAdapter
        implements WebMvcConfigurer, WebApplicationInitializer {

    @Autowired
    @NotNull
    private UserDetailsService userDetailsService;

    @Bean
    @NotNull
    public Endpoint authEndpointSOAPRegistry(
            @NotNull final AuthEndpointSOAP controller,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final Endpoint endpoint = new EndpointImpl(cxf, controller);
        endpoint.publish("/AuthEndpointSOAP");
        return endpoint;
    }

    @Override
    protected void configure(@NotNull final AuthenticationManagerBuilder auth)
            throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/ws/*").permitAll()
                .antMatchers("/api/auth/*").permitAll()
                .anyRequest().fullyAuthenticated()
                .and()
                .authorizeRequests()
                .and()
                .formLogin()
                .defaultSuccessUrl("/")
                .and()
                .logout()
                .logoutRequestMatcher(
                        new AntPathRequestMatcher("/logout")
                )
                .permitAll()
                .logoutSuccessUrl("/login")
                .deleteCookies("JSESSIONID")
                .invalidateHttpSession(true).permitAll()
                .and()
                .csrf().disable();
    }

    @Bean
    @NotNull
    public SpringBus cxf() {
        return new SpringBus();
    }

    @Override
    public void onStartup(@NotNull final ServletContext servletContext)
            throws ServletException {
        @NotNull final CXFServlet cxfServlet = new CXFServlet();
        @NotNull final ServletRegistration.Dynamic dynamic =
                servletContext.addServlet("cxfServlet", cxfServlet);
        dynamic.addMapping("/ws/*");
        dynamic.setLoadOnStartup(1);
    }

    @Bean
    @NotNull
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @NotNull
    public Endpoint projectEndpointRegistry(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final Endpoint endpoint =
                new EndpointImpl(cxf, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    @NotNull
    public ViewResolver resolver() {
        @NotNull final InternalResourceViewResolver resolver =
                new InternalResourceViewResolver();
        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    @Bean
    @NotNull
    public Endpoint taskEndpointRegistry(
            @NotNull final TaskEndpoint controller,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final Endpoint endpoint = new EndpointImpl(cxf, controller);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    @NotNull
    public Endpoint userEndpointRegistry(
            @NotNull final UserEndpoint controller,
            @NotNull final SpringBus cxf
    ) {
        @NotNull final Endpoint endpoint = new EndpointImpl(cxf, controller);
        endpoint.publish("/UserEndpoint");
        return endpoint;
    }

}
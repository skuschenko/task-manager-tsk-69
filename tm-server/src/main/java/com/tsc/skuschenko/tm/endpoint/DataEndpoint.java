package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.service.IDataService;
import com.tsc.skuschenko.tm.api.service.dto.ISessionDTOService;
import com.tsc.skuschenko.tm.dto.SessionDTO;
import com.tsc.skuschenko.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@WebService
public class DataEndpoint extends AbstractEndpoint {

    @NotNull
    @Autowired
    private IDataService dataService;
    @NotNull
    @Autowired
    private ISessionDTOService sessionDTOService;

    @WebMethod
    @SneakyThrows
    public void loadBackupCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        sessionDTOService.validate(session, Role.ADMIN);
        dataService.loadBackupCommand();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataBase64Command(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        sessionDTOService.validate(session, Role.ADMIN);
        dataService.loadDataBase64Command();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataBinaryCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        sessionDTOService.validate(session, Role.ADMIN);
        dataService.loadDataBinaryCommand();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataJsonFasterXmlCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        sessionDTOService.validate(session, Role.ADMIN);
        dataService.loadDataJsonFasterXmlCommand();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataJsonJaxBCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        sessionDTOService.validate(session, Role.ADMIN);
        dataService.loadDataJsonJaxBCommand();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataXmlFasterXmlCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        sessionDTOService.validate(session, Role.ADMIN);
        dataService.loadDataXmlFasterXmlCommand();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataXmlJaxBCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        sessionDTOService.validate(session, Role.ADMIN);
        dataService.loadDataXmlJaxBCommand();
    }

    @WebMethod
    @SneakyThrows
    public void loadDataYamlFasterXmlCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        sessionDTOService.validate(session, Role.ADMIN);
        dataService.loadDataYamlFasterXmlCommand();
    }

    @WebMethod
    @SneakyThrows
    public void saveBackupCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        sessionDTOService.validate(session, Role.ADMIN);
        dataService.saveBackupCommand();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataBase64Command(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        sessionDTOService.validate(session, Role.ADMIN);
        dataService.saveDataBase64Command();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataBinaryCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        sessionDTOService.validate(session, Role.ADMIN);
        dataService.saveDataBinaryCommand();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataJsonFasterXmlCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        sessionDTOService.validate(session, Role.ADMIN);
        dataService.saveDataJsonFasterXmlCommand();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataJsonJaxBCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        sessionDTOService.validate(session, Role.ADMIN);
        dataService.saveDataJsonJaxBCommand();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataXmlFasterXmlCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        sessionDTOService.validate(session, Role.ADMIN);
        dataService.saveDataXmlFasterXmlCommand();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataXmlJaxBCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        sessionDTOService.validate(session, Role.ADMIN);
        dataService.saveDataXmlJaxBCommand();
    }

    @WebMethod
    @SneakyThrows
    public void saveDataYamlFasterXmlCommand(
            @WebParam(name = "session", partName = "session")
            @Nullable final SessionDTO session
    ) {
        sessionDTOService.validate(session, Role.ADMIN);
        dataService.saveDataYamlFasterXmlCommand();
    }

}
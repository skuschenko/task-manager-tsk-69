package com.tsc.skuschenko.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.management.ManagementFactory;
import java.text.SimpleDateFormat;
import java.util.Date;

public interface SystemUtil {

    @NotNull
    static String getCurrentDateTime() {
        @NotNull final String pattern = "dd.mm.yyyy HH:MM:SS";
        @NotNull final SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat(pattern);
        @NotNull final String date = simpleDateFormat.format(new Date());
        return date;
    }

    static long getPID() {
        @Nullable final String processName =
                ManagementFactory.getRuntimeMXBean().getName();
        if (processName != null && processName.length() > 0) {
            try {
                return Long.parseLong(processName.split("@")[0]);
            } catch (@NotNull final Exception e) {
                return 0;
            }
        }
        return 0;
    }

}

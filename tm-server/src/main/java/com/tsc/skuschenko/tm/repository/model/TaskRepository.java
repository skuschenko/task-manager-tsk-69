package com.tsc.skuschenko.tm.repository.model;

import com.tsc.skuschenko.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TaskRepository extends AbstractRepository<Task> {

    @Modifying
    @Query("DELETE FROM Task e")
    void clearAllTasks();

    @Query("SELECT e FROM Task e")
    @Nullable List<Task> findAll();

    @Query("SELECT e FROM Task e WHERE e.project.getId() = :projectId")
    @Nullable List<Task> findAllTaskByProjectId(
            @Param("projectId") @NotNull String projectId
    );

    @Query("SELECT e FROM Task e WHERE e.id = :id")
    @Nullable Task findOneById(@Param("id") @NotNull String id);

    @Query("SELECT e FROM Task e")
    @Nullable List<Task> findOneByIndex();

    @Query("SELECT e FROM Task e WHERE e.name = :name")
    @Nullable Task findOneByName(@Param("name") @NotNull String name);

    @Query("SELECT e FROM Task e WHERE e.id = :id")
    @Nullable Task findTaskById(@Param("id") @NotNull String id);

    @Modifying
    @Query("DELETE FROM Task e WHERE e.project.getId() = :projectId")
    void removeOneById(@Param("projectId") @NotNull String projectId);

}
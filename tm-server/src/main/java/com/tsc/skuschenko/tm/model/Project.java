package com.tsc.skuschenko.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tsc.skuschenko.tm.api.entity.IWBS;
import com.tsc.skuschenko.tm.listener.EntityListener;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Entity
@Setter
@NoArgsConstructor
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "tm_project")
@EntityListeners(EntityListener.class)
public final class Project extends AbstractBusinessEntity implements IWBS {

    @JsonIgnore
    @OneToMany(mappedBy = "project")
    @Nullable
    private List<Task> tasks = new ArrayList<>();

}

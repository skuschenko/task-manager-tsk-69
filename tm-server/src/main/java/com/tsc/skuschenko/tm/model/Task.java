package com.tsc.skuschenko.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tsc.skuschenko.tm.api.entity.IWBS;
import com.tsc.skuschenko.tm.listener.EntityListener;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "tm_task")
@EntityListeners(EntityListener.class)
public final class Task extends AbstractBusinessEntity implements IWBS {

    @JsonIgnore
    @ManyToOne
    @Nullable
    private Project project;

}
